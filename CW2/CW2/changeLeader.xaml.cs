﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK   
    /// THIS IS THE CLASS THAT WILL ALLOW YOU TO CHANGE THE LEADER OF A MODULE
    /// </summary>
    public partial class changeLeader : Window
    {
        public changeLeader()
        {
            InitializeComponent();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //This button will take you back to the main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //the following code will make sure a module with the module code provided exists
                string pathModule = @"C:\\Staff.txt";
                TextReader readerModule = new StreamReader(pathModule);
                string line1;
                int moduleExists = 0;
                while ((line1 = readerModule.ReadLine()) != null)
                {
                    //this will split each line up on the tab into an array that will be used to find the wanted ro
                    var rows = line1.Split('\t');
                    //if the 1st row contains the content of the leader text box it will change the check module variable to one 
                    if (rows[0].ToLower().Equals(txt_leader.Text.ToLower())) { moduleExists = 1; }
                }

                readerModule.Close();
                //if the module wasnt found code will show this message
                if (moduleExists != 1) { MessageBox.Show("There is no module leader with that name"); }
                //check fields not empty
                else if (txt_code.Text.Equals("") || txt_leader.Text.Equals("")) { MessageBox.Show("Please fill in all the fields"); }
                else
                {
                    string line;
                    //i will be used as a counter to figure out which line needs to be changed
                    int i = 0;
                    //module not found will remain at one if a module isnt found then a message will be displayed
                    int moduleNotFound = 1;
                    string path = @"C:\\module.txt";
                    TextReader reader = new StreamReader(path);
                    while ((line = reader.ReadLine()) != null)
                    {
                        var rows = line.Split('\t');
                        //when the code equals the first row from the file it will change the line it equaled on
                        if (rows[1].Equals(txt_code.Text))
                        {
                            reader.Close();
                            var file = File.ReadAllLines(path);
                            //this file will change the line and then the text file will be re written with the changed line
                            file[i] = (rows[0] + "\t" + rows[1] + "\t" + txt_leader.Text);
                            File.WriteAllLines(path, file);
                            MessageBox.Show("Module Leader has been changed");
                            moduleNotFound = 0;
                            break;
                        }
                        i++;
                    }
                    if (moduleNotFound == 1)
                    {
                        MessageBox.Show("Module with that code wasn't found");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Module with that code wasn't found");
            }
        }
    }
}
