﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK   
    /// THIS WILL ALLOW USERS TO INPUT NEW MODULES TO BE USED LATER
    /// </summary>
    public partial class ModuleInfo : Window
    {
        public ModuleInfo()
        {
            InitializeComponent();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            //check to see if module leader already exists
            string pathStaff = @"C:\\staff.txt";
            TextReader readerStaff = new StreamReader(pathStaff);
            string line1;
            int leaderExists = 0;
            while ((line1 = readerStaff.ReadLine()) != null)
            {
                var rows = line1.Split('\t');
                if (rows[0].Equals(txt_moduleLeader.Text.ToLower())) { leaderExists = 1; }
            }
            readerStaff.Close();
            if (leaderExists != 1) { MessageBox.Show("There is no member of staff with that name. Please check the list fo staff option"); }
            //check to see if module code is unique
            else
            {
                string path = @"C:\\Module.txt";
                TextReader reader = new StreamReader(path);
                string line;
                int uniqueCheck = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    var rows = line.Split('\t');
                    if (rows[1] == txt_moduleCode.Text) { uniqueCheck = 1; }
                }
                if (uniqueCheck == 1) { MessageBox.Show("Module with this module code already exist"); }
                else
                {
                    reader.Close();
                    Module module = new Module();
                    //check fields are not empty
                    if (txt_moduleName.Text.Equals("") || txt_moduleCode.Text.Equals("") || txt_moduleLeader.Text.Equals(""))
                    {
                        MessageBox.Show("Please make sure all the field are filled in");
                    }
                    //save the values to the module class and then write a new line in a file with all the information.
                    else
                    {
                        module.ModuleName = txt_moduleName.Text;
                        module.ModuleCode = txt_moduleCode.Text;
                        module.ModuleLeader = txt_moduleLeader.Text;
                        FileStream fs1 = new FileStream(path, FileMode.Append);
                        StreamWriter writer = new StreamWriter(fs1);
                        writer.Write(module.ModuleName + "\t" + module.ModuleCode + "\t" + module.ModuleLeader + Environment.NewLine);
                        writer.Close();
                        MessageBox.Show("Module has been added");
                    }
                }
            }
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the add window
            Add addWin = new Add();
            this.Close();
            addWin.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take the users all the way back to the main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }
    }
}
