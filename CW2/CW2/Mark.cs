﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//JAKUB SOBCZAK
//THIS CLASS WILL CREATE THE VARIABLES TO STORE THE STUDENTS MARK AND STATUS AFTER THEY HAVE BEEN ENROLLED

namespace CW2
{
    class Mark
    {
        //the following are private variables which are linked to public methods that will allow other classes to access them
        private string enrolModuleCode;
        public string EnrolModuleCode
        {
            get
            {
                return (enrolModuleCode);
            }
            set
            {
                enrolModuleCode = value;
            }
        }
        private int studentMarticulation;
        public int StudentMatriculation
        {
            get
            {
                return (studentMarticulation);
            }
            set
            {
                studentMarticulation = value;
            }
        }
        private int mark;
        public int StudentMark
        {
            get
            {
                return (mark);
            }
            set
            {
                mark = value;
            }
        }
        private string status;
        public string Status
        {
            get
            {
                return (status);
            }
            set
            {
                status = value;
            }
        }
        //the following method uses an integer as an input to return a string value according to the students mark
        public string Result(int mark)
        {
            string status = "";
            if (mark == -1)
            {
                status = "Studying";
            }
            else if (mark >= 40)
            {
                status = "Passed";
            }
            else { 
                status = "Failed"; 
            }
            return status;
        }
    }
}
