﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// Interaction logic for List.xaml
    /// </summary>
    public partial class List : Window
    {
        public List()
        {
            InitializeComponent();
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }

        private void btn_listStudents_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the window that will list all the students
          ListStudents students = new ListStudents();
          this.Close();
          students.ShowDialog();
        }

        private void btn_listStaff_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the window that will list the staff members
            ListStaff staff = new ListStaff();
            this.Close();
            staff.ShowDialog();
        }

        private void btn_listModules_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the window that lists modules
            modules Modules = new modules();
            this.Close();
            Modules.ShowDialog();
        }
    }
}
