﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK
    /// THIS WINDOW WILL ALLOW A STUDENT TO BE ENROLED ON A MODULE
    /// </summary>
    public partial class Enrol : Window
    {
        public Enrol()
        {
            InitializeComponent();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            //this will catch the error that occurs if user enters string instead of an int
            try
          {
                //make sure module with that module code exists
                string pathModule = @"C:\\Module.txt";
                TextReader readerModule = new StreamReader(pathModule);
                string line1;
                int moduleExists = 0;
                while ((line1 = readerModule.ReadLine()) != null)
                {
                    var rows = line1.Split('\t');
                    if (rows[1].Equals(txt_moduleCode.Text)) { moduleExists = 1; }
                }
         
                readerModule.Close();
                if (moduleExists != 1) { MessageBox.Show("There is no module with that code. Try adding a new module"); }
                else
                 {  
                    //make sure the student with that matriculation number exists
                    string pathStaff = @"C:\\Students.txt";
                    TextReader readerStaff = new StreamReader(pathStaff);
                    string line2;
                    int studentExists = 0;
                    while ((line2 = readerStaff.ReadLine()) != null)
                    {
                        var rows = line2.Split('\t');
                        if (rows[3].Equals(txt_matriculation.Text)) { studentExists = 1; }
                    }
                    readerStaff.Close();
                    if (studentExists != 1) { MessageBox.Show("There is no student with that matriculation number. Try adding a new student"); }
                    else
                    {
                        //make sure student hasn't been enroled on the module before
                        string uniqueCheck = txt_moduleCode.Text + txt_matriculation.Text;
                        string path = @"C:\\mark.txt";
                        TextReader reader = new StreamReader(path);
                        string line;
                        int checkResult = 0;
                        while ((line = reader.ReadLine()) != null)
                        {
                            var rows = line.Split('\t');
                            if (rows[4].Equals(uniqueCheck)) { checkResult = 1; }
                        }
                        reader.Close();
                        if (checkResult == 1) { MessageBox.Show("This student is already enroled on this module."); }
                        else
                        {
                            int noMark = -1;
                            Mark mark = new Mark();
                            //Check to make sure fields are filled in
                            if (txt_moduleCode.Text.Equals("") || txt_moduleCode.Text.Equals("") || txt_matriculation.Text.Equals(""))
                            {
                                MessageBox.Show("Please fill in all the fields.");
                            }

                            else
                            {
                                mark.EnrolModuleCode = txt_moduleCode.Text;
                                mark.StudentMatriculation = Convert.ToInt32(txt_matriculation.Text);
                                mark.StudentMark = noMark;
                                mark.Status = "Studying";

                                FileStream fs1 = new FileStream(path, FileMode.Append);
                                StreamWriter writer = new StreamWriter(fs1);
                                //combined module code and student matriculation number to uniquely identify if a student is already enroled on a module
                                writer.Write(mark.EnrolModuleCode + "\t" + mark.StudentMatriculation + "\t" + mark.StudentMark + "\t" + mark.Status + "\t" + mark.EnrolModuleCode + mark.StudentMatriculation + Environment.NewLine);
                                writer.Close();
                                MessageBox.Show("Student was enroled.");
                            }
                        }
                    }
                }
            }
            catch { MessageBox.Show("Please use integers."); }
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            Add addWin = new Add();
            this.Close();
            addWin.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }
    }
}
