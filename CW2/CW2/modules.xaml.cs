﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK
    /// THIS CLASS WILL DISPLAY THE MODULE INFORMATION
    /// </summary>
    public partial class modules : Window
    {
        public modules()
        {
            InitializeComponent();
            //as soon as the window is opened the textbox will be filled with all the modules
            string text = File.ReadAllText(@"C:\\Module.txt");
            txt_modules.Text = text;
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            List list = new List();
            this.Close();
            list.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }
    }
}
