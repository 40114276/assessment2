﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK
    /// THIS CLASS WILL SAVE THE STUDENT INFORMATION FOR LATER USE 
    /// </summary>
    public partial class StudentInfo : Window
    {

        Students students = new Students();

        public StudentInfo()
        {
            InitializeComponent();
        }


        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the add window
            Add addWin = new Add();
            this.Close();
            addWin.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the main menu window
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //check if email contains @ sign
                string email = txt_email.Text;
                bool b;
                b = email.Contains("@");
                //following code makes sure the matriculation number hasn't been saved before
                string path = @"C:\\students.txt";
                TextReader reader = new StreamReader(path);
                string line;
                int uniqueCheck = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    var rows = line.Split('\t');
                    if (rows[3].Equals(txt_matric.Text)) { uniqueCheck = 1; }
                }
                //checks the text boxes arent empty
                if (txt_name.Text.Equals("") || txt_address.Text.Equals("") || txt_email.Text.Equals("") || txt_matric.Text.Equals("")) { MessageBox.Show("Please make sure all the boxes are filled in."); }
                //if matriculation number check does already exist a message will be displayed
                else if (uniqueCheck == 1) { MessageBox.Show("Student with this matriculation number already exists"); }
                //if the email check comes back false a message will be displayed
                else if (b != true) { MessageBox.Show("Please enter a correct email format.(Needs to contain @)"); }
                //makes sure the matriculation number provided is within the acceptable values
                else if (Convert.ToInt32(txt_matric.Text) < 1000 || Convert.ToInt32(txt_matric.Text) > 9000) { MessageBox.Show("Matriculation Number must be between 1000 and 9000"); }
                //if the information entered passes all the checks the student will be saved in the student class and then entered as a new line to the student file
                else
                {
                    students.StudentName = txt_name.Text;
                    students.StudentAddress = txt_address.Text;
                    students.StudentEmail = txt_email.Text;
                    students.StudentMatriculationNumber = Convert.ToInt32(txt_matric.Text);

                    reader.Close();
                    FileStream fs1 = new FileStream(path, FileMode.Append);
                    StreamWriter writer = new StreamWriter(fs1);
                    writer.Write(students.StudentName + "\t" + students.StudentAddress + "\t" + students.StudentEmail + "\t" + students.StudentMatriculationNumber + Environment.NewLine);
                    writer.Close();
                    MessageBox.Show("Student has been added");
                }
            }
            //if string enterend instead of in then message will be displayed
            catch { MessageBox.Show("Please enter integers only for your matriculation number"); }
        }
    }
}

