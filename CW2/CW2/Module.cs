﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//JAKUB SOBCZAK 
//THIS CLASS WILL STORE THE MODULE VALUES BEFORE THEY ARE SAVED IN THE TEXT FILE
namespace CW2
{
    class Module
    {
        //the following are private variables which are linked to public methods that will allow other classes to access them
        private string moduleName;
        public string ModuleName
        {
            get
            {
                return (moduleName);
            }
            set
            {
                moduleName = value;
            }
        }
        private string moduleCode;
        public string ModuleCode
        {
            get
            {
                return (moduleCode);
            }
            set
            {
                moduleCode = value;
            }
        }
        private string moduleLeader;
        public string ModuleLeader
        {
            get
            {
                return (moduleLeader);
            }
            set
            {
                moduleLeader = value;
            }
        }
    }
}
