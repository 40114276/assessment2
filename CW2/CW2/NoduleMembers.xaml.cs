﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK
    /// THIS CLASS WILL SHOW THE MEMBERS ON THE MODULE THAT USER REQUESTS AS WELL AS THEIR MARKS AND STATUS
    /// </summary>
    public partial class NoduleMembers : Window
    {
        public NoduleMembers()
        {
            InitializeComponent();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the main window
            MainWindow main = new MainWindow();
            this.Close();
            main.ShowDialog();
        }

        private void btn_find_Click(object sender, RoutedEventArgs e)
        {
            //makes sure the textbox isn't empty
            if (txt_moduleCode.Text.Equals("")) { MessageBox.Show("Please enter a module code into the field provided."); }
            else
            {
                //if the module isnt found this will clear the field
                txt_members.Text = "";
                //the following code searches the mark file for the module code and displays all the students on that module
                string markPath = @"C:\\mark.txt";
                TextReader reader = new StreamReader(markPath);
                string line;
                List<string> list = new List<string>();
                int i = 0;
                int moduleFound = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    var rows = line.Split('\t');
                    if (rows[0].Equals(txt_moduleCode.Text))
                    {
                        txt_members.AppendText(rows[1] + "   " + rows[2] + "   " + rows[3] + Environment.NewLine);
                        i++;
                        moduleFound = 1;
                    }
                }
                //if the module isnt found this message will show up
                if (moduleFound == 0) { MessageBox.Show("This module wasn't found"); }
            }
        }
    }
}
