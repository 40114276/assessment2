﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK   
    /// THIS CLASS WILL ALLOW YOU TO EDIT STUDENT DETAILS AND DELETE THE STUDENTS
    /// </summary>
    public partial class EditStudent : Window
    {
        public EditStudent()
        {
            InitializeComponent();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            //this button will edit student details
            //emailCheck
            string email = txt_email.Text;
            bool b;
            b = email.Contains("@");

            string studentPath = @"C:\\students.txt";
            //checks that fields aren't empty
            if (txt_matric.Text.Equals("") || txt_name.Text.Equals("") || txt_address.Text.Equals("") || txt_email.Text.Equals(""))
            {
                MessageBox.Show("Plese fill in all the fields");
            }
            //uses the email check to make sure the email is in the correct format
            else if (b != true) { MessageBox.Show("Please enter a correct email format.(Needs to contain @)"); }
            else
            {
                //this try and catch will display a message if a string is used instead of an int
                try
                {
                //the following code reads the code line by line then seperates the lines by tab carracters
                //it the finds the  line matching the matriculation number and changes it
                    int matriculation = Convert.ToInt32(txt_matric.Text);
                    string line;
                    //i counts which line the code is on so it can then be changed
                    int i = 0;
                    //this variable will stay the same if the student wasn't found
                    int studentExists = 0;
                    TextReader reader = new StreamReader(studentPath);
                    while ((line = reader.ReadLine()) != null)
                    {
                        var rows = line.Split('\t');
                        if (rows[3].Equals(txt_matric.Text))
                        {
                            studentExists++;
                            reader.Close();
                            var file = File.ReadAllLines(studentPath);
                            file[i] = (txt_name.Text + "\t" + txt_address.Text + "\t" + txt_email.Text + "\t" + rows[3]);
                            File.WriteAllLines(studentPath, file);
                            MessageBox.Show("Student details have been changed");
                            break;
                        }
                        i++;
                    }
                    //if the student wasn't found this message will be displayed
                    if (studentExists == 0) { MessageBox.Show("This student doesn't exist"); }
                }
                catch { MessageBox.Show("Please enter an integer as the matriculation number"); }
            }
        }


        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the main window
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }

        private void btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            //this button will find the student requested and delete his details
            string studentPath = @"C:\\students.txt";
            //checks to make sure the field isn't empty
            if (txt_matric.Text.Equals(""))
            {
                MessageBox.Show("Please enter the matriculation number of the student you want to delete.");
            }

            else
            {
                //catches an error if a string is entered instead of an int
                try
                {
                    string line;
                    //i counts which line the code is on so it can be changed when needed
                    int i = 0;
                    //this int will stay the same if the student isnt found and an error message will then be displayed
                    int studentExists = 0;
                    TextReader reader = new StreamReader(studentPath);
                    //this time when the programme finds the line with the correct matriculation number it will delete it from the array and then rewritte the file
                    while ((line = reader.ReadLine()) != null)
                    {
                        var rows = line.Split('\t');
                        if (rows[3].Equals(txt_matric.Text))
                        {
                            studentExists++;
                            reader.Close();
                            var file = File.ReadAllLines(studentPath);
                            var newFile = file.ToList();
                            newFile.Remove(file[i]);
                            File.WriteAllLines(studentPath, newFile);
                            MessageBox.Show("Student has been deleted");
                            //after the file is written there is nothing left to do so it will break
                            break;
                        }
                        i++;
                    }
                    if (studentExists == 0) { MessageBox.Show("This student doesn't exist"); }
                }
                catch { MessageBox.Show("Please enter an integer as the matriculation number"); }
            }
        }
    }
}
