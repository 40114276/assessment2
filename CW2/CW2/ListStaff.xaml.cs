﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK   
    /// THIS CLASS LISTS ALL THE STAFF MEMBERS IN A TEXT BOX
    /// </summary>
    public partial class ListStaff : Window
    {
        public ListStaff()
        {
            InitializeComponent();
            //as soon as the programme lunches it will load the content of the file into the text box
            string text = File.ReadAllText(@"C:\\staff.txt");
            txt_staff.Text = text;
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the window that will allow you to list modules and students
            List list = new List();
            this.Close();
            list.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you all the way back to the main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }
    }
}
