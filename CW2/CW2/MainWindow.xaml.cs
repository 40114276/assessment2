﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK   
    /// THIS IS THE MAIN WINDOW CLASS THAT WILL GRANT ACCESS TO ALL THE OTHER WINDOWS
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //when this window lunches it will check if the required files exist and if they dont it will create them
            string pathStudents = @"C:\\Students.txt";
            string pathMark = @"C:\\Mark.txt";
            string pathStaff = @"C:\\Staff.txt";
            string pathModule = @"C:\\Module.txt";

            if (!File.Exists(pathStudents))
            {
                StreamWriter sw = File.AppendText(pathStudents);
                sw.Close();
            }
            if (!File.Exists(pathMark))
            {
                StreamWriter sw = File.AppendText(pathMark);
                sw.Close();
            }
            if (!File.Exists(pathStaff))
            {
                StreamWriter sw = File.AppendText(pathStaff);
                sw.Close();
            }
            if (!File.Exists(pathModule))
            {
                StreamWriter sw = File.AppendText(pathModule);
                sw.Close();
            }
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the add window
            Add addWin = new Add();
            this.Close();
            addWin.ShowDialog();
        }

        private void btn_list_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the list window
            List listWin = new List();
            this.Close();
            listWin.ShowDialog();
        }

        private void btn_enrol_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the enrol window
            Enrol enrol = new Enrol();
            this.Close();
            enrol.ShowDialog();
        }

        private void btn_studentMark_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the edit window
            EditMark edit = new EditMark();
            this.Close();
            edit.ShowDialog();
        }

        private void btn_editStudent_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the edit student window
            EditStudent editStudent = new EditStudent();
            this.Close();
            editStudent.ShowDialog();
        }

        private void btn_listMembers_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the window that will list module members
            NoduleMembers moduleMembers = new NoduleMembers();
            this.Close();
            moduleMembers.ShowDialog();
        }

        private void btn_reassign_Click(object sender, RoutedEventArgs e)
        {
            //this will take you to the leader window
            changeLeader leader = new changeLeader();
            this.Close();
            leader.ShowDialog();
        }
    }
}
