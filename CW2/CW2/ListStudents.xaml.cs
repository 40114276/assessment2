﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK   
    /// THIS CLASS WILL LIST ALL THE STUDENTS
    /// </summary>
    public partial class ListStudents : Window
    {
        public ListStudents()
        {
            InitializeComponent();
            //as soon as the window lunches it will read the text file into the textbox
            string text = File.ReadAllText(@"C:\\students.txt");
            txt_students.Text = text;
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the list window
            List list = new List();
            this.Close();
            list.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you all the way back to the main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }
    }
}
