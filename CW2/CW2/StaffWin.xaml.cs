﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK
    /// THIS CLASS WILL ALLOW YOU TO ADD ALL THE STAFF MEMBERS 
    /// </summary>
    public partial class StaffWin : Window
    {
        public StaffWin()
        {
            InitializeComponent();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {   
         
            //check to see if payroll number is unique
            string path = @"C:\\Staff.txt";
            TextReader reader = new StreamReader(path);
            string line;
            int uniqueCheck = 0;
            while ((line = reader.ReadLine()) != null)
            {
                var rows = line.Split('\t');
                if (rows[5].Equals(txt_staffPay.Text)) { uniqueCheck = 1; }
            }
            reader.Close();
            string email = txt_staffEmail.Text;
            //email check
            bool b;
            b = email.Contains("@");
            Staff staff = new Staff();
            //checks to make sure the text boxes arent empty
            if (txt_staffName.Text.Equals("") || txt_staffAddress.Text.Equals("") || txt_staffEmail.Text.Equals("") || txt_staffPay.Text.Equals("") || txt_staffDepartment.Text.Equals("") || txt_staffRole.Text.Equals(""))
            {
                MessageBox.Show("Please make sure each value is fille in");
            }
            //if the unique check doesnt come back as 0 a message will be displayed
            else if (uniqueCheck == 1) { MessageBox.Show("Staff member with this payroll number already exists"); }
            //checks to make sure the payroll number is within the given guidelines
            else if (Convert.ToInt32(txt_staffPay.Text) < 9000 || Convert.ToInt32(txt_staffPay.Text) > 9999) { MessageBox.Show("Please enter a payroll number value between 9000 and 9999"); }
            //if email check comes back falls message will be displayed
            else if (b != true) { MessageBox.Show("Please enter a correct email format.(Needs to contain @)"); }
            //makes sure the staff role entered is one of the 3 given values
            else if (txt_staffRole.Text.ToLower().Equals("lecturer") || txt_staffRole.Text.ToLower().Equals("senior lecturer") || txt_staffRole.Text.ToLower().Equals("professor")) 
            {
                //if all of the checks pass then the staff members is saved in the variables then a new line created in the staff file
                try
                {
                        staff.StaffName = txt_staffName.Text;
                        staff.StaffAddress = txt_staffAddress.Text;
                        staff.StaffEmail = txt_staffEmail.Text;
                        staff.StaffDepartment = txt_staffDepartment.Text;
                        staff.StaffRole = txt_staffRole.Text;
                        staff.StaffPayrollNumber = Convert.ToInt32(txt_staffPay.Text);
                        FileStream fs1 = new FileStream(path, FileMode.Append);
                        StreamWriter writer = new StreamWriter(fs1);
                        writer.Write(staff.StaffName + "\t" + staff.StaffAddress + "\t" + staff.StaffEmail + "\t" + staff.StaffDepartment + "\t" + staff.StaffRole + "\t" + staff.StaffPayrollNumber + Environment.NewLine);
                        writer.Close();
                        MessageBox.Show("Staff member saved");
                }
                //catches the error message if string is entered instead 
                catch { MessageBox.Show("Please enter an integer for the payroll number"); }
            }
            else
            {
                { MessageBox.Show("Please make sure you enter the correct value into the role box"); }
            }
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the add window
            Add addWin = new Add();
            this.Close();
            addWin.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you all the way back to the main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }
    }
}
