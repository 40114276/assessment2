﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

//JAKUB SOBCZAK 
//FOLLOWING CLASS WILL STORE THE STUDENT VARIABLES BEFORE THEY ARE SAVED IN A FILE
namespace CW2
{
    class Students
    {
        //the following are private variables which are linked to public methods that will allow other classes to access them
        private string name;

        public string StudentName
        {
            get
            {
                return (name);
            }
            set
            {
                name = value;
            }
        }
        private string address;
        public string StudentAddress
        {
            get
            {
                return (address);
            }
            set
            {
                address = value;
            }
        }
        private string email;
        public string StudentEmail
        {
            get
            {
                return (email);
            }
            set
            {
                email = value;
            }
        }
        private int matriculationNumber;
        public int StudentMatriculationNumber
        {
            get
            {
                return (matriculationNumber);
            }
            set
            {
                matriculationNumber = value;
            }
        }
    }
}
