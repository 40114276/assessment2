﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

/*
 * JAKUB SOBCZAK 
 * THIS CLASS WILL ALLOW YOU TO EDIT A STUDENTS MARK. THE STUDENT HAS TO BE ENROLED FIRST
*/
namespace CW2
{
    public partial class EditMark : Window
    {
        public EditMark()
        {
            InitializeComponent();
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to the add window
            Add addWin = new Add();
            this.Close();
            addWin.ShowDialog();
        }

        private void btn_main_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you all the way back to the main menu
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            //if the programm crashed due to text being entered instead of an int this try will catch it and display an error message
            try
            {
                Mark mark = new Mark();
                //these int checks are here to make sure the programme crashes if an text is inputed
                int check1 = Convert.ToInt32(txt_matriculation.Text);
                int check2 = Convert.ToInt32(txt_mark.Text);
                string path = @"C:\\mark.txt";
                //checks that the mark entered is within the required values
                if (check2 < 0 || check2 > 100) { MessageBox.Show("Please enter a value for the mark between 0 and 100"); }
                else
                {
                    //makes sure the text boxes aren't left empty
                    if (txt_mark.Text != "" || txt_matriculation.Text != "")
                    {
                        string line;
                        int i = 0;
                        //if this int stays the same a message will show up saying student hasn't been found
                        int studentNotFound = 0;
                        TextReader reader = new StreamReader(path);
                        while ((line = reader.ReadLine()) != null)
                        {   
                            //this will split each line into an array that so the programm can search the file by each row
                            var rows = line.Split('\t');
                            if (rows[1].Equals(txt_matriculation.Text))
                            {
                                
                                reader.Close();
                                var file = File.ReadAllLines(path);
                                //once the required line has been found it will be changed and then the text file will be rewritten with the change
                                file[i] = (rows[0] + "\t" + rows[1] + "\t" + txt_mark.Text.ToString() + "\t" + mark.Result(Convert.ToInt32(txt_mark.Text))+"\t"+rows[4]);
                                File.WriteAllLines(path, file);
                                MessageBox.Show("Student mark has been changed");
                                studentNotFound = 1;
                                break;
                            }
                            i++;
                        }
                        if (studentNotFound == 0) { MessageBox.Show("Student with that matriculation number hasn't been found. Maybe he hasn't been enroled on any modules?"); }
                    }
                    else
                    {
                        MessageBox.Show("Please fill in all the fields");
                    }
                }
            }
            catch { MessageBox.Show("Please use integers in the boxes"); }
        }
    }
}
