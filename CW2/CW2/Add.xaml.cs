﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW2
{
    /// <summary>
    /// JAKUB SOBCZAK
    /// THIS IS A WINDOW WITH BUTTONS TAKING YOU TO ADD STAFF, MODULES AND STUDENTS
    /// </summary>
    public partial class Add : Window
    {
        public Add()
        {
            InitializeComponent();
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you back to main window
            MainWindow mainWin = new MainWindow();
            this.Close();
            mainWin.ShowDialog();
        }

        private void btn_addStudent_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the add student window
            StudentInfo studentWin = new StudentInfo();
            this.Close();
            studentWin.ShowDialog();
        }

        private void btn_addStaff_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the add staff window
            StaffWin staffWin = new StaffWin();
            this.Close();
            staffWin.ShowDialog();
        }

        private void btn_addModule_Click(object sender, RoutedEventArgs e)
        {
            //this button will take you to the add module window
            ModuleInfo module = new ModuleInfo();
            this.Close();
            module.ShowDialog();
        }
    }
}
