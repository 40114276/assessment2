﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//JAKUB SOBCZAK 
//THIS CLASS WILL STORE ALL THE STAFF VARIABLES BEFORE THEY ARE UPLOADED ON A FILE
namespace CW2
{
    class Staff
    {
        //the following are private variables which are linked to public methods that will allow other classes to access them
        private string name;
        public string StaffName
        {
            get
            {
                return (name);
            }
            set
            {
                name = value;
            }
        }
        private string address;
        public string StaffAddress
        {
            get
            {
                return (address);
            }
            set
            {
                address = value;
            }
        }

        private string email;
        public string StaffEmail
        {
            get
            {
                return (email);
            }
            set
            {
                email = value;
            }
        }

        private int payrollNumber;
        public int StaffPayrollNumber
        {
            get
            {
                return (payrollNumber);
            }
            set
            {
                payrollNumber = value;
            }
        }

        private string department;
        public string StaffDepartment
        {
            get
            {
                return (department);
            }
            set
            {
                department = value;
            }
        }

        private string role;
        public string StaffRole
        {
            get
            {
                return (role);
            }
            set
            {
               role = value;
            }
        }

    }
}
